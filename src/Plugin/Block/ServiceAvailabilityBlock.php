<?php

namespace Drupal\service_availability\Plugin\Block;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Entity\Group;
use Drupal\node\NodeInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\service_availability\Utility\DateConversion;

/**
 * Provides a block displaying Service Availability.
 *
 * @Block(
 *   id = "service_availability_services",
 *   admin_label = @Translation("Service Availability")
 * )
 */
class ServiceAvailabilityBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The render array to pass to the theme.
   *
   * @var array
   */
  protected $render;

  /**
   * Time now as UTC.
   *
   * @var string
   */
  protected $nowUtc;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('current_route_match'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Constructs a new Service availability block instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $route_match, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $route_match;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $group_id = NULL;
    $node_storage = $this->entityTypeManager->getStorage('node');
    $entity = $this->routeMatch->getParameter('node');

    // Check this is a node.
    if (($entity instanceof NodeInterface)) {
      $entity_id = $entity->id();
      $group_id = $this->getGroupsFromEntityId($entity_id);
    }

    // If block is being displayed outside of a Group context show all services.
    if (empty($group_id)) {

      $entity_ids = $node_storage->getQuery()
        ->accessCheck(TRUE)
        ->condition('status', 1)
        ->condition('type', 'service')
        ->sort('title', 'ASC')
        ->execute();

      $services = $node_storage->loadMultiple($entity_ids);
    }
    else {
      $group = Group::load($group_id);
      $services = $group->getContent('group_node:service');
    }

    // No services created yet - nothing to display.
    if (empty($services)) {
      return;
    }

    // Set time to filter messages by as current time.
    $now = new DrupalDateTime('now');

    // Use same timezone as date range fields (UTC).
    $now->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));

    // Use same format as date range fields.
    $this->nowUtc = $now->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

    // Initialise the render array that will be passed to the theme.
    $this->render = [];
    $this->render['#theme'] = 'service_availability';
    $this->render['#attached'] = [
      'library' => [
        'service_availability/service-availability-service',
      ],
    ];

    foreach ($services as $service) {

      $current_status = 'normal';

      // Get Service ID.
      if ($service->hasField('entity_id')) {
        // Service ID while inside of Group context.
        $service_id = $service->get('entity_id')->getString();
        // Load the Service node instead of Group relationship to it.
        $service = $node_storage->load($service_id);
      }
      elseif ($service->hasField('nid')) {
        // Service ID outside of Group context.
        $service_id = $service->nid->getString();
      }

      if (isset($service_id)) {

        // Set the Service title which will display as a tab.
        $this->render['#services'][$service_id]['#theme'] = 'service_availability_service';
        $this->render['#services'][$service_id]['#service_label'] = $service->label();

        $message_ids = $node_storage->getQuery()
          ->accessCheck(TRUE)
          ->condition('type', 'service_message')
          ->condition('status', 1)
          ->condition('field_service.entity.nid', $service_id)
          ->condition('field_date_time.end_value', $this->nowUtc, '>=')
          ->sort('field_date_time', 'DESC')
          ->execute();

        // Process each Service Message for this Service.
        if ($message_ids) {

          $messages = $node_storage->loadMultiple($message_ids);

          foreach ($messages as $message) {
            $this->setDay($service_id, $message);
          }

          $this->sortMessages($service_id);
        }

        // Set Current Status for this Service.
        $service_status = $service->field_current_status->value;

        if (!empty($service_status) && $service_status != 'normal') {
          $current_status = $service_status;
          $message_format = ['label' => 'hidden'];

          if ($service->field_unplanned_outage_message) {
            $this->render['#services'][$service_id]['#current_status_messages'] = $service->field_unplanned_outage_message->view($message_format);
          }
        }
        elseif (!empty($this->render['#services'][$service_id]['#current_status_messages'])) {
          $current_message = current($this->render['#services'][$service_id]['#current_status_messages']);
          $current_status = $current_message['#impact'];
        }

        if (empty($current_status)) {
          $current_status = 'normal';
        }

        $this->validateUpcoming($service_id);
        $this->render['#services'][$service_id]['#current_status'] = $current_status;

      }
    }

    return $this->render;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['service_availability_services'] = $form_state->getValue('service_availability_services');
  }

  /**
   * Set to not cache the block.
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * Get ID of Group an entity is associated with.
   */
  private function getGroupsFromEntityId($entity_id) {
    $group_id = NULL;
    $group_content_storage = $this->entityTypeManager->getStorage('group_content');

    $ids = $group_content_storage->getQuery()
      ->accessCheck(TRUE)
      ->condition('entity_id', $entity_id)
      ->execute();

    /** @var \Drupal\group\Entity\GroupContentInterface[] $relations */
    $relations = $group_content_storage->loadMultiple($ids);

    foreach ($relations as $relation) {
      if ($relation->getEntity()->getEntityTypeId() == 'node') {
        $group_id = $relation->getGroup()->id();
      }
    }

    return $group_id;
  }

  /**
   * Set messages for a day.
   */
  private function setDay($service_id, $message) {

    $impact = $message->field_impact->value;
    $description = $message->field_description->value;

    // Convert from UTC to timezone used for display.
    $start = DateConversion::dateFromUtc($message->field_date_time->start_date);
    $end = DateConversion::dateFromUtc($message->field_date_time->end_date);

    $days = $this->days($start, $end);

    for ($day = 0; $day <= $days; $day++) {
      $end_date_time = clone $end;
      $hours = $this->hours($start, $end_date_time);
      $this->setMessage($service_id, $start, $end_date_time, $hours, $impact, $description);

      // Increment by one day.
      $start->modify('+1 day')->setTime(00, 00);
    }

  }

  /**
   * Add message to the render array.
   */
  private function setMessage($service_id, $start, $end, $hours, $impact, $description) {

    $day = $start->format('Y-m-d');
    $time = $start->format('H:i');

    $this->render['#services'][$service_id]['#days'][$day]['#messages'][$time] = [
      '#theme' => 'service_availability_message',
      '#date' => $start->format('j F Y'),
      '#time' => $hours,
      '#duration' => $this->duration($start, $end),
      '#impact' => $impact,
      '#description' => $description,
    ];

    $this->render['#services'][$service_id]['#days'][$day]['#theme'] = 'service_availability_day';
    $this->render['#services'][$service_id]['#days'][$day]['#date'] = $start->format('jS \o\f F');

    // If this message is current add to Current Status.
    $message = $this->render['#services'][$service_id]['#days'][$day]['#messages'][$time];
    $this->currentMessage($start, $end, $service_id, $message);
  }

  /**
   * Sort messages by date and time.
   */
  private function sortMessages($service_id) {
    // Sort messages within each day (earliest first).
    foreach ($this->render['#services'][$service_id]['#days'] as $key => $value) {
      // Check array key is a date.
      if ($key != '#theme' && $key != '#date') {
        ksort($this->render['#services'][$service_id]['#days'][$key]['#messages']);
      }
    }

    // Sort days ascending (closest to today first).
    ksort($this->render['#services'][$service_id]['#days']);
  }

  /**
   * Clear Upcoming when there is only one message and it becomes current.
   */
  private function validateUpcoming($service_id) {
    $upcoming = FALSE;

    if (isset($this->render['#services'][$service_id]['#days'])) {

      foreach ($this->render['#services'][$service_id]['#days'] as $key => $value) {
        // Check array key is a date.
        if ($key != '#theme' && $key != '#date') {
          // Check there are messages for this day.
          if (!empty($this->render['#services'][$service_id]['#days'][$key]['#messages'])) {
            $upcoming = TRUE;
          }
        }
      }
    }

    if (!$upcoming) {
      unset($this->render['#services'][$service_id]['#days']);
    }

  }

  /**
   * Calculate hours for each day of message.
   */
  private function hours($start, $end) {

    $days = $this->days($start, $end);

    // Message only lasts one day or this is last day.
    if ($days == 0) {
      $time = $start->format('H:i') . ' - ' . $end->format('H:i');
    }
    else {
      $time = $start->format('H:i') . ' - 24:00';
    }

    return $time;
  }

  /**
   * Calculate number of days a message is for.
   */
  private function days($start, $end) {
    $start_day = $start->format('z');
    $end_day = $end->format('z');
    $days = $end_day - $start_day;
    return $days;
  }

  /**
   * Calculate duration to display.
   */
  private function duration($start, $end) {

    $days = $this->days($start, $end);

    // Additional days for this message, use midnight as end time.
    if ($days) {
      $end->setDate($start->format('Y'), $start->format('m'), $start->format('d'));
      $end->setTime(24, 00);
    }

    $interval = $start->diff($end);
    $hours = ($interval->format("%a") * 24) + $interval->format("%h");

    if ($hours > 24) {
      $hours = 24;
    }

    $minutes = $interval->format("%i");

    // No minutes just show hours.
    if (empty($minutes)) {
      return $hours . ' hours';
    }

    // Half an hour abreviate to .5 of an hour.
    if ($minutes == 30) {
      return $hours . '.5 hours';
    }

    // Show hours and minutes.
    return $hours . ' ' . $this->t('hours') . ' ' . $minutes . ' ' . $this->t('minutes');
  }

  /**
   * Set Current Status if Service Message is for current time.
   */
  private function currentMessage($start, $end, $service_id, $message) {

    // Convert to timestamp.
    $start_time = strtotime($start->format('Y-m-d H:i:00'));
    $end_time = strtotime($end->format('Y-m-d H:i:00'));

    // Set current time to same timezone and format as start & end time.
    $now = DateConversion::dateFromUtc($this->nowUtc);
    $now = strtotime($now->format('Y-m-d H:i:00'));

    if ($now >= $start_time && $now <= $end_time) {
      $this->render['#services'][$service_id]['#current_status_messages'][$start_time] = $message;

      // Remove from Upcoming.
      $day = $start->format('Y-m-d');
      $time = $start->format('H:i');
      unset($this->render['#services'][$service_id]['#days'][$day]['#messages'][$time]);
    }
  }

}
