<?php

namespace Drupal\service_availability\Utility;

/**
 * Provides date conversions.
 */
class DateConversion {

  /**
   * Convert UTC dates to timezone used by the site.
   */
  public static function dateFromUtc($data) {
    $tzone = date_default_timezone_get();
    $date = new \DateTime($data, new \DateTimeZone('UTC'));
    $date->setTimezone(new \DateTimeZone($tzone));
    return $date;
  }

}
