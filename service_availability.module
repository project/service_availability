<?php

/**
 * @file
 * Provides Service Availability features.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\service_availability\Utility\DateConversion;

/**
 * Implements hook_theme().
 */
function service_availability_theme($existing, $type, $theme, $path) {
  return [
    'service_availability' => [
      'variables' => [
        'services' => NULL,
      ],
    ],
    'service_availability_service' => [
      'variables' => [
        'service_label' => '',
        'current_status' => '',
        'current_status_messages' => NULL,
        'days' => NULL,
        'images_path' => '/' . $path . '/images',
      ],
    ],
    'service_availability_day' => [
      'variables' => [
        'date' => '',
        'messages' => NULL,
      ],
    ],
    'service_availability_message' => [
      'variables' => [
        'date' => '',
        'time' => '',
        'duration' => '',
        'impact' => '',
        'description' => '',
        'service status' => '',
      ],
    ],
  ];
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function service_availability_form_node_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  // Simplify the Service Message create and edit form.
  if ($form_id == 'node_service_message_form' || $form_id == 'node_service_message_edit_form') {
    // Auto set title field for Service Message nodes.
    $form['title']['#access'] = FALSE;
    // Hide other unused form elements.
    $form['revision_information']['#access'] = FALSE;
    $form['#entity_builders'][] = 'service_availability_service_message_title_builder';
    $form['#after_build'][] = 'service_availability_menu_hide_after_build';
  }

  // Simplify the Service create and edit form.
  if ($form_id == 'node_service_form' || $form_id == 'node_service_edit_form') {
    // Hide other unused form elements.
    $form['revision_information']['#access'] = FALSE;
    $form['#after_build'][] = 'service_availability_menu_hide_after_build';
  }
}

/**
 * Hide Menu options on a node edit form.
 */
function service_availability_menu_hide_after_build(array $form, FormStateInterface $form_state) {
  $form['menu']['#access'] = FALSE;
  return $form;
}

/**
 * Title builder for Service Message content type.
 */
function service_availability_service_message_title_builder($entity_type, NodeInterface $node, $form, FormStateInterface $form_state) {
  $day = DateConversion::dateFromUtc($node->field_date_time->getValue()[0]['value']);
  $day = $day->format('jS \o\f F H:i');
  $node->setTitle('Outage on ' . $day);
}
