# Service Availability

Provides a display for the current status of one or more services and upcoming scheduled disruptions.

Features:

- Multiple services can be added
- Multiple upcoming messages can be added to each service
- Automatically calculates the duration of a disruption
- Upcoming scheduled messages are automatically moved to Current status based on date and time
- Scheduled messages are automatically removed from display once their scheduled time has passed
- Groups aware so Services and messages can be can be added per Group

For a full description of the module, visit the
[project page](https://www.drupal.org/project/service_availability).
Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/service_availability).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. 
For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Add the Service Availability block to pages where you would like to show the status of Services.


## Usage

Installing this module will automatically create two new content types:

- Service
  Create a Service for each service that you would like to show the status for
- Service Message
  Create a Service Message for each upcoming scheduled disruption to a Service

## Maintainers

Current maintainers:
- Robert Castelo - [robertcastelo](https://www.drupal.org/u/robert-castelo)


