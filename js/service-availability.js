/*
*  This software includes Open Source licensed material derived from:
*  https://www.w3.org/WAI/ARIA/apg/example-index/tabs/js/tabs-automatic.js
*
*   File:   tabs-automatic.js
*
*   Desc:   Tablist widget that implements ARIA Authoring Practices
*/
(function () {
  'use strict';

  Drupal.behaviors.serviceAvailability = {
    attach: function (context) {

      // Work around Drupal bug loading Toolbar script into context. 
      if (context.querySelector("#service-availability-tab-active") == null) {
        return;
      }

      // Basic tab functionality.
      class TabsAutomatic {
        constructor(groupNode) {
          this.tablistNode = groupNode;
      
          this.tabs = [];
      
          this.firstTab = null;
          this.lastTab = null;
      
          this.tabs = Array.from(this.tablistNode.querySelectorAll('[role=tab]'));
          this.tabpanels = [];
      
          for (var i = 0; i < this.tabs.length; i += 1) {
            var tab = this.tabs[i];
            var tabpanel = document.getElementById(tab.getAttribute('aria-controls'));
      
            tab.tabIndex = -1;
            tab.setAttribute('aria-selected', 'false');
            this.tabpanels.push(tabpanel);
      
            tab.addEventListener('keydown', this.onKeydown.bind(this));
            tab.addEventListener('click', this.onClick.bind(this));
      
            if (!this.firstTab) {
              this.firstTab = tab;
            }
            this.lastTab = tab;
          }
      
          this.setSelectedTab(this.firstTab, false);
        }
      
        setSelectedTab(currentTab, setFocus) {
          if (typeof setFocus !== 'boolean') {
            setFocus = true;
          }
          for (var i = 0; i < this.tabs.length; i += 1) {
            var tab = this.tabs[i];
            if (currentTab === tab) {
              tab.setAttribute('aria-selected', 'true');
              tab.removeAttribute('tabindex');
              this.tabpanels[i].classList.remove('hidden');
              if (setFocus) {
                tab.focus();
              }
            } else {
              tab.setAttribute('aria-selected', 'false');
              tab.tabIndex = -1;
              this.tabpanels[i].classList.add('hidden');
            }
          }
        }
      
        setSelectedToPreviousTab(currentTab) {
          var index;
      
          if (currentTab === this.firstTab) {
            this.setSelectedTab(this.lastTab);
          } else {
            index = this.tabs.indexOf(currentTab);
            this.setSelectedTab(this.tabs[index - 1]);
          }
        }
      
        setSelectedToNextTab(currentTab) {
          var index;
      
          if (currentTab === this.lastTab) {
            this.setSelectedTab(this.firstTab);
          } else {
            index = this.tabs.indexOf(currentTab);
            this.setSelectedTab(this.tabs[index + 1]);
          }
        }
      
        /* EVENT HANDLERS */
        onKeydown(event) {
          var tgt = event.currentTarget,
            flag = false;
      
          switch (event.key) {
            case 'ArrowLeft':
              this.setSelectedToPreviousTab(tgt);
              flag = true;
              break;
      
            case 'ArrowRight':
              this.setSelectedToNextTab(tgt);
              flag = true;
              break;
      
            case 'Home':
              this.setSelectedTab(this.firstTab);
              flag = true;
              break;
      
            case 'End':
              this.setSelectedTab(this.lastTab);
              flag = true;
              break;
      
            default:
              break;
          }
      
          if (flag) {
            event.stopPropagation();
            event.preventDefault();
          }
        }
      
        onClick(event) {
          this.setSelectedTab(event.currentTarget);
        }
      }

      var tablists = context.querySelectorAll('.block-service-availability [role=tablist].automatic');

      for (var i = 0; i < tablists.length; i++) {
        new TabsAutomatic(tablists[i]);
      }

      // Sliding tab underline.
      const tabs = context.querySelectorAll('.block-service-availability [role="tab"]');

      class TabSlider {
        constructor(initialOffset) {
          this.start = initialOffset;
          this.destination = initialOffset;
        }

        startSet(start) {
          this.start = start;
        }

        destinationSet(destination) {
          this.destination = destination;
        }
      }

      // Set width of slider to same as each tab.
      let underline = context.querySelector("#service-availability-tab-active");
      let firstButton = context.querySelector('.block-service-availability .tab-controls button');
      underline.style.width = firstButton.offsetWidth + 'px';

      let parent = context.querySelector('.block-service-availability .tab-controls');
      let slider = new TabSlider(firstButton.offsetLeft);

      for (var i=0; i < tabs.length; i++) {

        tabs[i].addEventListener('click', function () {

          slider.destinationSet(this.offsetLeft);
          let startPosition = slider.start;
          slider.startSet(slider.destination);
          let destinationPosition = slider.destination;
       
          underline.animate(
            [ 
              { transform: "translateX(" + (startPosition - parent.offsetLeft) + "px)" }, 
              { transform: "translateX(" + (destinationPosition - parent.offsetLeft) + "px)" }
            ], 
            {
              duration: 500, 
              easing: "ease-out",
              fill: "both"
            }
          );

        });
      }
    }
  };

})();